package br.com.drogaria01.test;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import br.com.drogaria01.dao.EstadoDAO;
import br.com.drogaria01.domain.Estado;

public class EstadoDAOTest {
	@Test
	public void salvar() {
		Estado estado = new Estado();
		EstadoDAO estadoDAO = new EstadoDAO();
		
		estado.setNome("São Paulo");
		estado.setSigla("SP");
		
		estadoDAO.salvar(estado);
	}
	
	@Test
	public void listar() {
		EstadoDAO estadoDAO = new EstadoDAO();
		List<Estado> resultado = estadoDAO.listar();
		
		System.out.println("Total Registros encontrados: " + resultado.size());
		
		for (Estado estado : resultado) {
			System.out.println(estado.getNome());
		}
	}
	
	@Test
	public void buscar() {
		Long codigo = 1L;
		EstadoDAO estadoDAO = new EstadoDAO();
		Estado estado = new Estado();
		
		estado = estadoDAO.buscar(codigo);
		System.out.println(estado.getNome());
	}
	
	@Test
	@Ignore
	public void excluir() {
		Long codigo = 1L;
		EstadoDAO estadoDAO = new EstadoDAO();
		Estado estado = estadoDAO.buscar(codigo);
		
		if (estado == null) {
			System.out.println("Nenhum registro encontrado");
		}
		else {
			estadoDAO.excluir(estado);
			System.out.println("Registro removido");
			System.out.println(estado.getNome());
		}
	}
	
	@Test
	public void editar() {
		Long codigo = 1L;
		EstadoDAO estadoDAO = new EstadoDAO();
		Estado estado = estadoDAO.buscar(codigo);
		
		if (estado == null) {
			System.out.println("Nenhum registro encontrado");
		} else {
			System.out.println("Registro editado - Antes:");
			System.out.println(estado.getCodigo() + " - " + estado.getSigla() + " - " + estado.getNome());
			
			estado.setNome("Santa Catarina");
			estado.setSigla("SC");
			estadoDAO.editar(estado);
			
			System.out.println("Registro editado - Depois:");
			System.out.println(estado.getCodigo() + " - " + estado.getSigla() + " - " + estado.getNome());
		}
	}

}

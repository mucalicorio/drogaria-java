package br.com.drogaria01.test;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import br.com.drogaria01.dao.CidadeDAO;
import br.com.drogaria01.domain.Cidade;

public class CidadeDAOTest {
	@Test
	public void salvar() {
		Cidade cidade = new Cidade();
		CidadeDAO cidadeDAO = new CidadeDAO();
		
		cidade.setNome("Quintana");
		
		cidadeDAO.salvar(cidade);
	}
	
	@Test
	@Ignore
	public void listar() {
		CidadeDAO cidadeDAO = new CidadeDAO();
		List<Cidade> resultado = cidadeDAO.listar();
		
		System.out.println("Total Registros encontrados: " + resultado.size());
		
		for (Cidade cidade : resultado) {
			System.out.println(cidade.getNome());
		}
	}
	
	@Test
	public void buscar() {
		Long codigo = 1L;
		CidadeDAO cidadeDAO = new CidadeDAO();
		Cidade cidade = new Cidade();
		
		cidade = cidadeDAO.buscar(codigo);
		System.out.println(cidade.getNome());
	}
	
	@Test
	@Ignore
	public void excluir() {
		Long codigo = 1L;
		CidadeDAO cidadeDAO = new CidadeDAO();
		Cidade cidade = cidadeDAO.buscar(codigo);
		
		if (cidade == null) {
			System.out.println("Nenhum registro encontrado");
		} else {
			cidadeDAO.excluir(cidade);
			System.out.println("Registro removido");
			System.out.println(cidade.getNome());
		}
	}
	
	@Test
	public void editar() {
		Long codigo = 10L;
		CidadeDAO cidadeDAO = new CidadeDAO();
		Cidade cidade = cidadeDAO.buscar(codigo);
		
		if (cidade == null) {
			System.out.println("Nenhum registro encontrado");
		} else {
			System.out.println("Registro editado - Antes:");
			System.out.println(cidade.getCodigo() + " - " + cidade.getNome());
			cidade.getNome();
			
			cidade.setNome("Pompéia");
			cidadeDAO.editar(cidade);
			
			System.out.println("Registro editado - Depois:");
			System.out.println(cidade.getCodigo() + " - " + cidade.getNome());
		}
	}

}
